# README #

## Usuarios por defecto ##

Al arrancar la aplicación, hay 3 usuarios creados por defecto con la siguiente definición:
```
    {
        "Id": "945a595f-eab0-4223-bccd-b59e74a04cb8",
        "UserName": "testuser2",
        "Email": "testuser2@schibsted.com",
        "PasswordHash": "ALIHB5ymahJwgAqygHDvZke8eGWG4USnbXfXVCizQuIYTsaN2ne93tY7D20AZl4gCw==",
        "Roles": [
            "PAGE_2",
            "ADMIN"
        ]
    }
```
```
    {
        "Id": "945a595f-eab0-4223-bccd-b59e74a04cb7",
        "UserName": "testuser1",
        "Email": "testuser1@schibsted.com",
        "PasswordHash": "AMzJTgF18FsmLndoK7FyFdvwDZk3aKzYnz/UKIAm2pqTxzQV3pY3X2WJbVMkKsHLyA==",
        "Roles": [
            "PAGE_1"
        ]
    }
```
```
    {
        "Id": "945a595f-eab0-4223-bccd-b59e74a04cb9",
        "UserName": "testuser3",
        "Email": "testuser3@schibsted.com",
        "PasswordHash": "AB8eVNJeQ0SngflmXUWei+SwsUNkET+2c4DfKewZwmf5Tuwg5sxQKCW+sdETfEc6Mw==",
        "Roles": [
            "PAGE_1",
            "PAGE_3"
        ]
    }
```

El **password** en todos los casos **equivale al UserName**.

## Portal ##

* Mientras no se esté logado en la aplicación, el acceso a cualquier página te redirige al login. Al autenticarse, te redirige automáticamente a la página a la que se estaba intentando acceder.
* Al autenticarse desde el login directamente (sin página de destino), te redirige por defecto a la página Page1.
* En caso de estar autenticado y no tener el role necesario para acceder a alguna de las páginas, la aplicación retornara un error 403 (Forbidden).

## REST API ##


A continuación se muestran todos los endpoints con su definición para el CRUD de usuarios (Basic Auth):

* Obtener todos los usuarios:
```
GET /api/users
Authorization: Basic dGVzdHVzZXIyOnRlc3R1c2VyMg==
```

* Obtener usuario por id:
```
GET /api/users/{userId}
Authorization: Basic dGVzdHVzZXIyOnRlc3R1c2VyMg==
```

* Actualizar usuario concreto:
```
PUT /api/users/{userId}
Authorization: Basic dGVzdHVzZXIyOnRlc3R1c2VyMg==
Content-Type: application/json

Parámetros:
ApplicationUser

Request ejemplo:
{ "Id": "945a595f-eab0-4223-bccd-b59e74a04cb8", "UserName": "testuser4", "Email": "testuser4@schibsted.com", "PasswordHash": "ALIHB5ymahJwgAqygHDvZke8eGWG4USnbXfXVCizQuIYTsaN2ne93tY7D20AZl4gCw==", "Roles": [ "PAGE_2", "ADMIN" ] }
```

* Crear usuario:
```
POST /api/users/{userId}
Authorization: Basic dGVzdHVzZXIyOnRlc3R1c2VyMg==
Content-Type: application/json

Parámetros:
ApplicationUser
Password

Request ejemplo:
{"ApplicationUser": { "UserName": "testuser5", "Email": "testuser5@schibsted.com", "Roles": [ "PAGE_3", "ADMIN" ] }, "Password":"asdasd" }
```

* Eliminar usuario por id:
```
DELETE /api/users/{userId}
Authorization: Basic dGVzdHVzZXIyOnRlc3R1c2VyMg==
```