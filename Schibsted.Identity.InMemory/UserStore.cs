﻿using Schibsted.Identity.Core;
using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Threading.Tasks;

namespace Schibsted.Identity.InMemory
{
    public class UserStore<TUser> : IdentityUserStore<TUser>
        where TUser : IdentityUser
    {
        private static readonly ConcurrentDictionary<string, TUser> _users = new ConcurrentDictionary<string, TUser>();

        public UserStore()
        {
            Seed();
        }

        private async void Seed()
        {
            if (_users.Any()) return;

            var instance = Activator.CreateInstance<TUser>();
            instance.Id = "945a595f-eab0-4223-bccd-b59e74a04cb7";
            instance.UserName = "testuser1";
            instance.Email = "testuser1@schibsted.com";
            instance.PasswordHash = "AMzJTgF18FsmLndoK7FyFdvwDZk3aKzYnz/UKIAm2pqTxzQV3pY3X2WJbVMkKsHLyA==";
            await AddToRoleAsync(instance, "PAGE_1");
            await CreateAsync(instance);

            instance = Activator.CreateInstance<TUser>();
            instance.Id = "945a595f-eab0-4223-bccd-b59e74a04cb8";
            instance.UserName = "testuser2";
            instance.Email = "testuser2@schibsted.com";
            instance.PasswordHash = "ALIHB5ymahJwgAqygHDvZke8eGWG4USnbXfXVCizQuIYTsaN2ne93tY7D20AZl4gCw==";
            await AddToRoleAsync(instance, "PAGE_2");
            await AddToRoleAsync(instance, "ADMIN");
            await CreateAsync(instance);

            instance = Activator.CreateInstance<TUser>();
            instance.Id = "945a595f-eab0-4223-bccd-b59e74a04cb9";
            instance.UserName = "testuser3";
            instance.Email = "testuser3@schibsted.com";
            instance.PasswordHash = "AB8eVNJeQ0SngflmXUWei+SwsUNkET+2c4DfKewZwmf5Tuwg5sxQKCW+sdETfEc6Mw==";
            await AddToRoleAsync(instance, "PAGE_1");
            await AddToRoleAsync(instance, "PAGE_3");
            await CreateAsync(instance);
        }

        #region IQueryableUserStore

        public override IQueryable<TUser> Users
        {
            get { return _users.Values.AsQueryable(); }
        }

        #endregion

        #region IUserStore

        public override async Task<TUser> FindByIdAsync(string userId)
        {
            ThrowIfDisposed();
            TUser user;
            _users.TryGetValue(userId, out user);

            return await Task.FromResult(user);
        }

        public override async Task<TUser> FindByNameAsync(string userName)
        {
            ThrowIfDisposed();
            return await Task.FromResult(_users.Values.FirstOrDefault(u => u.UserName == userName));
        }

        public override async Task CreateAsync(TUser user)
        {
            ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException("user");

            var added = _users.TryAdd(user.Id, user);
            if (!added)
                throw new ArgumentException("Item with same Key has already been added", "user");
            
            await Task.FromResult(0);
        }

        public override async Task UpdateAsync(TUser user)
        {
            ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException("user");

            var oldUser = await FindByIdAsync(user.Id);
            var updated = _users.TryUpdate(user.Id, user, oldUser);
            if (!updated)
                throw new Exception("The user has not been updated.");

            await Task.FromResult(0);
        }

        public override async Task DeleteAsync(TUser user)
        {
            ThrowIfDisposed();
            if (user == null)
                throw new ArgumentNullException("user");

            TUser deletedUser;
            var deleted = _users.TryRemove(user.Id, out deletedUser);
            if (!deleted)
                throw new Exception("The user has not been removed.");

            await Task.FromResult(0);
        }

        #endregion

        #region IUserEmailStore

        public override async Task<TUser> FindByEmailAsync(string email)
        {
            ThrowIfDisposed();
            return await Task.FromResult(_users.Values.FirstOrDefault(u => u.Email == email));
        }

        #endregion
    }
}
