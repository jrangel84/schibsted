﻿using Schibsted.Identity.InMemory;
using Schibsted.TechnicalTest.Site.Controllers;
using Schibsted.TechnicalTest.Site.DTO;
using Schibsted.TechnicalTest.Site.Models;
using System.Linq;
using System.Net;
using System.Web.Http.Results;
using Xunit;

namespace Schibsted.TechnicalTest.Site.Tests.Controllers
{
    public class UsersControllerTest
    {
        private readonly ApplicationUserManager _userManager;

        public UsersControllerTest()
        {
            _userManager = new ApplicationUserManager(new UserStore<ApplicationUser>());
        }

        [Fact]
        public void ShouldGetUsers()
        {
            //arrange
            var usersController = new UsersController(_userManager);
            //act
            var users = usersController.GetUsers();
            //assert
            Assert.True(users.Any());
        }

        [Fact]
        public async void ShouldGetUser()
        {
            //arrange
            var usersController = new UsersController(_userManager);
            //act
            var result = await usersController.GetApplicationUser("945a595f-eab0-4223-bccd-b59e74a04cb7") as OkNegotiatedContentResult<ApplicationUser>;
            //assert
            Assert.NotNull(result);
            Assert.Equal(result.Content.UserName, "testuser1");
        }

        [Fact]
        public async void ShouldNotGetUser()
        {
            //arrange
            var usersController = new UsersController(_userManager);
            //act
            var result = await usersController.GetApplicationUser("999");
            //assert
            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public async void ShouldPutUser()
        {
            //arrange
            var usersController = new UsersController(_userManager);
            //act
            var applicationUser = new ApplicationUser
            {
                Id = "945a595f-eab0-4223-bccd-b59e74a04cb7",
                UserName = "testuser1",
                Email = "testuser1@schibsted.com",
                PasswordHash = "AMzJTgF18FsmLndoK7FyFdvwDZk3aKzYnz/UKIAm2pqTxzQV3pY3X2WJbVMkKsHLyA=="
            };
            var result = await usersController.PutApplicationUser("945a595f-eab0-4223-bccd-b59e74a04cb7", applicationUser);
            //assert
            Assert.NotNull(result);
            Assert.IsType<StatusCodeResult>(result);
            Assert.Equal((result as StatusCodeResult).StatusCode, HttpStatusCode.NoContent);
        }

        [Fact]
        public async void ShouldBadPutUser()
        {
            //arrange
            var usersController = new UsersController(_userManager);
            //act
            var applicationUser = new ApplicationUser
            {
                Id = "999",
                UserName = "testuser1",
                Email = "testuser1@schibsted.com",
                PasswordHash = "AMzJTgF18FsmLndoK7FyFdvwDZk3aKzYnz/UKIAm2pqTxzQV3pY3X2WJbVMkKsHLyA=="
            };
            var result = await usersController.PutApplicationUser("945a595f-eab0-4223-bccd-b59e74a04cb7", applicationUser);
            //assert
            Assert.IsType<BadRequestResult>(result);
        }

        [Fact]
        public async void ShouldNotPutUser()
        {
            //arrange
            var usersController = new UsersController(_userManager);
            //act
            var applicationUser = new ApplicationUser
            {
                Id = "999",
                UserName = "testuser9",
                Email = "testuser1@schibsted.com",
                PasswordHash = "AMzJTgF18FsmLndoK7FyFdvwDZk3aKzYnz/UKIAm2pqTxzQV3pY3X2WJbVMkKsHLyA=="
            };
            var result = await usersController.PutApplicationUser("999", applicationUser);
            //assert
            Assert.IsType<NotFoundResult>(result);
        }

        [Fact]
        public async void ShouldPostUser()
        {
            //arrange
            var usersController = new UsersController(_userManager);
            //act
            var applicationUser = new ApplicationUser
            {
                UserName = "testuser5",
                Email = "testuser1@schibsted.com"
            };
            var request = new PostUserRequest {ApplicationUser = applicationUser, Password = "password"};
            var result = await usersController.PostApplicationUser(request) as CreatedAtRouteNegotiatedContentResult<ApplicationUser>;
            //assert
            Assert.NotNull(result);
            Assert.Equal(result.Content.UserName, "testuser5");
        }

        [Fact]
        public async void ShouldBadPostUser()
        {
            //arrange
            var usersController = new UsersController(_userManager);
            //act
            var applicationUser = new ApplicationUser
            {
                Id = "945a595f-eab0-4223-bccd-b59e74a04cb7",
                UserName = "testuser1",
                Email = "testuser1@schibsted.com",
                PasswordHash = "AMzJTgF18FsmLndoK7FyFdvwDZk3aKzYnz/UKIAm2pqTxzQV3pY3X2WJbVMkKsHLyA=="
            };
            var request = new PostUserRequest { ApplicationUser = applicationUser, Password = string.Empty };
            var result = await usersController.PostApplicationUser(request);
            //assert
            Assert.IsType<BadRequestResult>(result);
        }

        [Fact]
        public async void ShouldConflictPostUser()
        {
            //arrange
            var usersController = new UsersController(_userManager);
            //act
            var applicationUser = new ApplicationUser
            {
                Id = "945a595f-eab0-4223-bccd-b59e74a04cb7",
                UserName = "testuser1",
                Email = "testuser1@schibsted.com",
                PasswordHash = "AMzJTgF18FsmLndoK7FyFdvwDZk3aKzYnz/UKIAm2pqTxzQV3pY3X2WJbVMkKsHLyA=="
            };
            var request = new PostUserRequest { ApplicationUser = applicationUser, Password = "password" };
            var result = await usersController.PostApplicationUser(request);
            //assert
            Assert.IsType<ConflictResult>(result);
        }

        [Fact]
        public async void ShouldDeleteUser()
        {
            //arrange
            var usersController = new UsersController(_userManager);
            //act
            var result = await usersController.DeleteApplicationUser("945a595f-eab0-4223-bccd-b59e74a04cb8") as OkNegotiatedContentResult<ApplicationUser>;
            //assert
            Assert.NotNull(result);
            Assert.Equal(result.Content.UserName, "testuser2");
        }

        [Fact]
        public async void ShouldNotDeleteUser()
        {
            //arrange
            var usersController = new UsersController(_userManager);
            //act
            var result = await usersController.DeleteApplicationUser("999");
            //assert
            Assert.IsType<NotFoundResult>(result);
        }
    }
}
