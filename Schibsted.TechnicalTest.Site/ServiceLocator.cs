﻿using LightInject;

namespace Schibsted.TechnicalTest.Site
{
    public class ServiceLocator
    {
        public static ServiceLocator Instance { get; private set; }
        private readonly IServiceContainer _container;

        public static void SetCurrentInjector(IServiceContainer container)
        {
            Instance = new ServiceLocator(container);
        }

        private ServiceLocator(IServiceContainer container)
        {
            _container = container;
        }

        public T GetService<T>()
        {
            return _container.GetInstance<T>();
        }
    }
}