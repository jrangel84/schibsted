﻿using Microsoft.AspNet.Identity.Owin;
using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using Schibsted.TechnicalTest.Site.Models;

namespace Schibsted.TechnicalTest.Site.FilterAttributes
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = true)]
    public class BasicAuthenticationAttribute : AuthorizeAttribute
    {
        public ApplicationUserManager UserManager
        {
            get { return HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>(); }
        }

        public override async Task OnAuthorizationAsync(HttpActionContext actionContext, CancellationToken cancellationToken)
        {
            var identity = ParseAuthorizationHeader(actionContext);
            if (identity == null)
            {
                HandleUnauthorizedRequest(actionContext);
                return;
            }

            if (!await OnAuthorizeUser(identity.Name, identity.Password))
            {
                HandleUnauthorizedRequest(actionContext);
            }
        }

        protected virtual async Task<bool> OnAuthorizeUser(string username, string password)
        {
            var user = await UserManager.FindAsync(username, password);
            if (user == null) return false;

            return await IsInRole(user);
        }

        protected virtual BasicAuthenticationIdentity ParseAuthorizationHeader(HttpActionContext actionContext)
        {
            string authHeader = null;
            var auth = actionContext.Request.Headers.Authorization;
            if (auth != null && auth.Scheme == "Basic")
                authHeader = auth.Parameter;

            if (string.IsNullOrEmpty(authHeader))
                return null;

            authHeader = Encoding.Default.GetString(Convert.FromBase64String(authHeader));

            var tokens = authHeader.Split(':');
            if (tokens.Length < 2)
                return null;

            return new BasicAuthenticationIdentity(tokens[0], tokens[1]);
        }

        protected override void HandleUnauthorizedRequest(HttpActionContext actionContext)
        {
            var host = actionContext.Request.RequestUri.DnsSafeHost;
            actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Forbidden);
            actionContext.Response.Headers.Add("WWW-Authenticate", string.Format("Basic realm=\"{0}\"", host));
        }

        private async Task<bool> IsInRole(ApplicationUser user)
        {
            var rolesSplit = SplitString(Roles);
            var isInRole = rolesSplit.Length <= 0;
            foreach (var role in rolesSplit)
            {
                isInRole |= await UserManager.IsInRoleAsync(user.Id, role);
            }

            return isInRole;
        }

        private static string[] SplitString(string original)
        {
            if (string.IsNullOrEmpty(original))
                return new string[0];

            return original.Split(',').Where(x => !string.IsNullOrEmpty(x.Trim())).ToArray();
        }
    }

    public class BasicAuthenticationIdentity : GenericIdentity
    {
        public BasicAuthenticationIdentity(string name, string password)
            : base(name, "Basic")
        {
            Password = password;
        }

        public string Password { get; set; }
    }
}