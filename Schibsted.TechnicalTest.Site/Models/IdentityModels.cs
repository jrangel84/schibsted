﻿using Microsoft.AspNet.Identity;
using Schibsted.Identity.Core;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Schibsted.TechnicalTest.Site.Models
{
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            return userIdentity;
        }
    }
}