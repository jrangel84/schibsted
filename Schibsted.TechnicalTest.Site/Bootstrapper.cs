﻿using LightInject;
using Microsoft.AspNet.Identity;
using Schibsted.TechnicalTest.Site.Models;
using System.Web.Http;

namespace Schibsted.TechnicalTest.Site
{
    public class Bootstrapper
    {
        private ServiceContainer _container;

        public Bootstrapper()
        {
            CreateContainer();
            RegisterServices();
        }

        public void CreateContainer()
        {
            _container = new ServiceContainer();
            ServiceLocator.SetCurrentInjector(_container);
        }

        protected void RegisterServices()
        {
            _container.Register<IUserStore<ApplicationUser>, Identity.InMemory.UserStore<ApplicationUser>>();
        }

        public void Initialize()
        {
            _container.RegisterApiControllers();
            _container.EnableWebApi(GlobalConfiguration.Configuration);
        }
    }
}