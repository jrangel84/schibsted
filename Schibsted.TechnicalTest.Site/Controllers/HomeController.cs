﻿using System.Web.Mvc;
using Schibsted.TechnicalTest.Site.FilterAttributes;

namespace Schibsted.TechnicalTest.Site.Controllers
{
    public class HomeController : Controller
    {
        [CustomAuthorize(Roles = "PAGE_1")]
        public ActionResult Page1()
        {
            return View();
        }

        [CustomAuthorize(Roles = "PAGE_2")]
        public ActionResult Page2()
        {
            return View();
        }

        [CustomAuthorize(Roles = "PAGE_3")]
        public ActionResult Page3()
        {
            return View();
        }
    }
}