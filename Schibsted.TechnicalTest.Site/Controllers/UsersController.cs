﻿using Microsoft.AspNet.Identity.Owin;
using Schibsted.TechnicalTest.Site.DTO;
using Schibsted.TechnicalTest.Site.FilterAttributes;
using Schibsted.TechnicalTest.Site.Models;
using System;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;

namespace Schibsted.TechnicalTest.Site.Controllers
{
    [BasicAuthentication]
    public class UsersController : ApiController
    {
        private ApplicationUserManager _userManager;

        public UsersController()
        {
        }

        public UsersController(ApplicationUserManager userManager)
        {
            UserManager = userManager;
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        // GET: api/Users
        public IQueryable<ApplicationUser> GetUsers()
        {
            return UserManager.Users;
        }

        // GET: api/Users/5
        [ResponseType(typeof(ApplicationUser))]
        public async Task<IHttpActionResult> GetApplicationUser(string id)
        {
            var applicationUser = await UserManager.FindByIdAsync(id);
            if (applicationUser == null)
            {
                return NotFound();
            }

            return Ok(applicationUser);
        }

        // PUT: api/Users/5
        [ResponseType(typeof(void))]
        [BasicAuthentication(Roles = "ADMIN")]
        public async Task<IHttpActionResult> PutApplicationUser(string id, ApplicationUser applicationUser)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (applicationUser == null || id != applicationUser.Id)
            {
                return BadRequest();
            }

            try
            {
                var result = await UserManager.UpdateAsync(applicationUser);
                if (!result.Succeeded)
                {
                    return InternalServerError();
                }
            }
            catch (Exception)
            {
                if (!(ApplicationUserExists(id)).Result)
                {
                    return NotFound();
                }
                throw;
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Users
        [ResponseType(typeof(ApplicationUser))]
        [BasicAuthentication(Roles = "ADMIN")]
        public async Task<IHttpActionResult> PostApplicationUser(PostUserRequest request)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (request == null || request.ApplicationUser == null || string.IsNullOrEmpty(request.Password))
            {
                return BadRequest();
            }

            try
            {
                var result = await UserManager.CreateAsync(request.ApplicationUser, request.Password);
                if (!result.Succeeded)
                {
                    return InternalServerError();
                }
            }
            catch (Exception)
            {
                if (ApplicationUserExists(request.ApplicationUser.Id).Result)
                {
                    return Conflict();
                }
                throw;
            }

            return CreatedAtRoute("DefaultApi", new { id = request.ApplicationUser.Id }, request.ApplicationUser);
        }

        // DELETE: api/Users/5
        [ResponseType(typeof(ApplicationUser))]
        [BasicAuthentication(Roles = "ADMIN")]
        public async Task<IHttpActionResult> DeleteApplicationUser(string id)
        {
            var applicationUser = await UserManager.FindByIdAsync(id);
            if (applicationUser == null)
            {
                return NotFound();
            }

            var result = await UserManager.DeleteAsync(applicationUser);

            if (!result.Succeeded)
            {
                return InternalServerError();
            }

            return Ok(applicationUser);
        }

        private async Task<bool> ApplicationUserExists(string id)
        {
            var applicationUser = await UserManager.FindByIdAsync(id);
            return applicationUser != null;
        }
    }
}