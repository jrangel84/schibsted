﻿using Schibsted.TechnicalTest.Site.Models;

namespace Schibsted.TechnicalTest.Site.DTO
{
    public class PostUserRequest
    {
        public ApplicationUser ApplicationUser { get; set; }
        public string Password { get; set; }
    }
}