﻿using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace Schibsted.TechnicalTest.Site
{
    public class MvcApplication : System.Web.HttpApplication
    {
        private static Bootstrapper _bootstrapper;
        protected void Application_Start()
        {
            _bootstrapper = new Bootstrapper();
            _bootstrapper.Initialize();

            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }
    }
}
