﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Schibsted.TechnicalTest.Site.Startup))]
namespace Schibsted.TechnicalTest.Site
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}