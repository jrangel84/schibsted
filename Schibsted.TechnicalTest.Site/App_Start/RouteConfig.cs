﻿using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Schibsted.TechnicalTest.Site
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            var controllerActionsConstraint = new ControllerActionsConstraint();

            routes.MapRoute(
                name: "Account",
                url: "{action}",
                defaults: new { controller = "Account", action = "Login" },
                constraints: new { action = controllerActionsConstraint }
            );

            routes.MapRoute(
                name: "Home",
                url: "{action}",
                defaults: new { controller = "Home" },
                constraints: new { action = controllerActionsConstraint }
            );
        }
    }

    public class ControllerActionsConstraint : IRouteConstraint
    {
        private readonly Dictionary<string, HashSet<string>> _controllerActions;

        public ControllerActionsConstraint()
        {
            _controllerActions = new Dictionary<string, HashSet<string>>();

            foreach (var descriptor in Assembly.GetCallingAssembly()
                                               .GetTypes()
                                               .Where(type => type.IsSubclassOf(typeof(Controller)))
                                               .Select(controller => new ReflectedControllerDescriptor(controller)))
            {
                _controllerActions[descriptor.ControllerName] = new HashSet<string>();
                foreach (var action in descriptor.GetCanonicalActions())
                {
                    _controllerActions[descriptor.ControllerName].Add(action.ActionName.ToLowerInvariant());
                }
            }
        }

        public bool Match(HttpContextBase httpContext, Route route, string parameterName, RouteValueDictionary values, RouteDirection routeDirection)
        {
            if (!_controllerActions.ContainsKey(values["controller"] as string ?? "")) return false;

            return _controllerActions[values["controller"] as string].Contains((values["action"] as string ?? "").ToLowerInvariant());
        }
    }
}
