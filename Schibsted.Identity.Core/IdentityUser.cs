﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;

namespace Schibsted.Identity.Core
{
    public class IdentityUser : IUser
    {
        public IdentityUser()
        {
            Id = Guid.NewGuid().ToString();
            Roles = new List<string>();
        }

        public string Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string PasswordHash { get; set; }
        public virtual ICollection<string> Roles { get; private set; }
    }
}
